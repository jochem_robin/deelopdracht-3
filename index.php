<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBVolley - Home</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">
			<div class="jumbotron">	
				<h1>Welkom</h1>
				<i class="fa fa-quote-left fa-5x pull-left fa-border"></i>
				<h3><strong>Wat doet de MBV?</strong> </h3>
				<p>De MBV organiseert een volleybalcompetitie voor teams van:Bedrijven, Buurt- / Sportverenigingen, vrije teams, enz, enz. </p>
				<h3><strong>Waar en wanneer?</strong></h3>
				<p>De wedstrijden worden op dinsdagavond in de sporthal "De Kruitmolen" te Middelburg gespeeld. </p>
				<h3><strong>Spelregels</strong></h3>
				<p>Er wordt gespeeld volgens de NEVOBO-spelregels. </p>
				<p>Er zijn echter uitzonderingen! </p>
				<ol>
				 <li>Er wordt op tijd gespeeld.</li>
				 <li>Er worden maximaal 4 sets gespeeld. </li>
				 </ol>
				<h3><strong>Inlichtingen </strong> </h3>
				<p> Wilt u informatie over de MBV-competitie dan kunt u kontakt opnemen </p>
				<p>Met de secretsresse: 0118-626280 of de wedstrijdleider: 0118-615289 </p>
			</div>
		</main>
	</body>
</html>
 