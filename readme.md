Er is ��n gebruiker aangemaakt in de database:
userid: admin
password: 1234

Een vergelijkbare competitie systeem is: http://mbvvolley.nl. De regels daar kun je als uitgangspunt gebruiken.

Known issues:


1. Er zijn dingen die een niet ingelogde gebruiker kan zien die die niet hoort te zien, bijv  de link 'uitslag' bij bepaalde wedstrijden.

   Joey & Delano
   

2. Per speelweek horen alle rondes en velden zichtbaar te zijn, ook als er geen wedstrijden ingepland zijn dan.

   Nienke & Adit & Matthijs
   

3. Uitslagen pagina klopt niet: er staan uitslagen in de database, maar zijn niet zichtbaar.

   Arnoudt & Milan
   

4. Verschillende gegevens, zoals leden en wedstrijden, kunnen niet worden ingevoerd door de admin.

   Bas & Sophie


5. Idee: kunnen wedstrijden niet worden 'gegenereerd' door een algoritme, aan het begin van het speelseizoen?

   Robin & Gert-Jan
   

6. Gegevens kunnen ook niet worden bewerkt.

   Robin & Jochem
   

7. Wens: elk lid krijgt ook een account. Als hij inlogt: iets als een "mijn team".

   -
   