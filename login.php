<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

	// controleer of uitloggen
	if (isset($_GET['action']) && $_GET['action']==='logoff') {
		session_destroy();
		//Een header sturen
		header('Location: wedstrijdschema.php');
		exit;
	}

	$loginTry = isset($_POST['userid']) && isset($_POST['passwd']);
	// Controleer op inloggen 
	if ($loginTry) {
		$username = strip_tags($_POST['userid']);
		$passwd = strip_tags($_POST['passwd']);
		if (authenticateUser($username, $passwd)) {
			//Een header sturen
			header('Location: wedstrijdschema.php');
			exit;
		}
	}
	
		
	// anders form laten zien
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<?php
			if ($loginTry && !isAuthenticated()) {
				echo '<span class="error">Login niet succesvol. Probeer opnieuw</span>';
			}
			?>
			<form method="post" action="login.php" style="width: 450px; margin-left: auto; margin-right: auto">
				<table >
					<tr>
						 <td valign="top">
						  	<label for="userid">Gebruikersnaam</label>
						 </td>
						 <td valign="top">
						  	<input  type="text" name="userid" maxlength="50" size="30">
						 </td>
					</tr>
					<tr>
						 <td valign="top">
						  	<label for="passwd">Wachtwoord</label>
						 </td>
						 <td valign="top">
						  	<input  type="password" name="passwd" value="" maxlength="50" size="30">
						 </td>
					</tr>
					<tr>
						 <td valign="top"></td>
						 <td valign="top">
						  	<input type="submit" name="commit" value="Login">
						 </td>
					</tr>
				</table>	
			</form>
		</main>
	</body>
</html>
