<html>
    <body>
        <main class="container">
            <?php include 'head.html' ?>
            <?php include 'header.php' ?>
            <div class="well">
                <?php
                $con=mysqli_connect("localhost", "root", "", "mbvolley");
                // Check connection
                if (mysqli_connect_errno()) {
                    echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

                $result = mysqli_query($con, "SELECT * FROM team");

                echo "<table border='1'>
                <table class='table table-striped table-condensed'>
                <th colspan='2' class='text-uppercase text-center'><div class='panel-heading'>Overzicht</div></th>
                <tr class='text-uppercase'>
                <th>Teams</th>
                <th class='text-right'>Klasse</th>
                </tr>";

                while ($row = mysqli_fetch_array($result)) {
                    echo "<tr>";
                    echo "<td>" . $row['naam'] . "</td>";

                    echo "<td class='text-right'>" . $row['klasse'] . "</td>";
                }
                echo "</table>";

                mysqli_close($con);
                ?>
            </div>

            <div class="well">
                <?php
                $team = array("Munter Volley 1","De Graaf Assurantien","Sport2000 Mdb/'t Znd","Koperarm","Autobedrijf Schout Gapinge","Jonika","VNLS","NNB","Fysio for You");
                shuffle($team);
                echo "<table border='1'>
                <table class='table table-striped table-condensed'>
                <th colspan='3' class='text-uppercase text-center'><div class='panel-heading'>Heren 1</div></th>
                <tr class='text-uppercase'>
                <th>Team A</th>
                <th>Team B</th>
                <th class='text-right'>Veld</th>
                </tr>";
                $random_keys=array_rand($team, 9);
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 2 ."</td>";
                echo "</table>";
                ?>
            </div>

            <div class="well">
                <?php
                $team = array("Eastman","Bowling de Kruitmolen / VV MZ (Heren)","Klaas Vaak","SMA Zeeland","HZ University of Applied Sciences","CZ","Provincie Zeeland","Munter Volley 2","University College Roosevelt");
                shuffle($team);
                echo "<table border='1'>
                <table class='table table-striped table-condensed'>
                <th colspan='3' class='text-uppercase text-center'><div class='panel-heading'>Heren 2</div></th>
                <tr class='text-uppercase'>
                <th>Team A</th>
                <th>Team B</th>
                <th class='text-right'>Veld</th>
                </tr>";
                $random_keys=array_rand($team, 9);
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[8]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[7]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 3 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 4 ."</td>";
                echo "</table>";
                ?>
            </div>

            <div class="well">
                <?php
                $team = array("Restaurant Valkenisse","The Tube","V.N.L.S.","Kraamzorg Post","T Ken Net 1","Vlijtig Liesje","VC Dauwendaele");
                shuffle($team);
                echo "<table border='1'>
                <table class='table table-striped table-condensed'>
                <th colspan='3' class='text-uppercase text-center'><div class='panel-heading'>Dames 1</div></th>
                <tr class='text-uppercase'>
                <th>Team A</th>
                <th>Team B</th>
                <th class='text-right'>Veld</th>
                </tr>";
                $random_keys=array_rand($team, 7);
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 5 ."</td>";
                echo "</table>";
                ?>
            </div>

            <div class="well">
                <?php
                $team = array("T Ken Net 2","Jumbo","Bowling de Kruitmolen / VV MZ (Dames)","Campingshop Clarijs / S.V.S. 2","Kookboerderij Krommehoeke","VV Jong Ambon","J2T / S.V.S. 1");
                shuffle($team);
                echo "<table border='1'>
                <table class='table table-striped table-condensed'>
                <th colspan='3' class='text-uppercase text-center'><div class='panel-heading'>Dames 2</div></th>
                <tr class='text-uppercase'>
                <th>Team A</th>
                <th>Team B</th>
                <th class='text-right'>Veld</th>
                </tr>";
                $random_keys=array_rand($team, 7);
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[5]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[4]]."</td>";
                echo "<td>" . $team[$random_keys[6]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[3]]."</td>";
                echo "<td class='text-right'>" . 6 ."</td>";
                echo "</table>";
                ?>
            </div>
            
            <!-- BEGIN TEMPLATE VOOR 3 TEAMS -->
            <div class="well">
                <?php
                $team = array("SMA Zeeland","Bowling d Kruitmolen","Munter Volley 2");
                shuffle($team);
                echo "<table border='1'>
                <table class='table table-striped table-condensed'>
                <th colspan='3' class='text-uppercase text-center'><div class='panel-heading'>Heren 2 (Template voor 3 Teams)</div></th>
                <tr class='text-uppercase'>
                <th>Team A</th>
                <th>Team B</th>
                <th class='text-right'>Veld</th>
                </tr>";
                $random_keys=array_rand($team, 3);
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[1]]."</td>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "<tr>";
                echo "<td>" . $team[$random_keys[2]]."</td>";
                echo "<td>" . $team[$random_keys[0]]."</td>";
                echo "<td class='text-right'>" . 1 ."</td>";
                echo "</table>";
                ?>
            </div>
            <!-- EINDE TEMPLATE VOOR 3 TEAMS -->

            <div class="well">
                <a class="btn btn-info" href="generate.php">Genereer Wedstrijden Opnieuw</a>
            </div>
        </main>
    </body>
</html>