<!DOCTYPE html>
<html lang="en">
<head>
    <title>MBVolley - Home</title>
    <?php include 'head.html' ?>
</head>
<body>
<?php include 'header.php' ?>
<div class="container">
    <div class="jumbotron">
        <h1><i class="fa fa-ban red"></i> MBVolley</h1>
        <p class="lead">Sorry! U bent op een pagina terecht gekomen waar u geen rechten heeft<em><span id="display-domain"></span></em>.</p>
    </div>
</div>
<div class="container">
    <div class="body-content">
        <div class="row">
            <div class="col-md-6">
                <h2>Wat gebeurd er?</h2>
                <p class="lead">U bent op een pagina voor de administrator terecht gekomen, u heeft geen rechten op deze pagina.</p>
            </div>
            <div class="col-md-6">
                <h2>Wat nu?</h2>
                <p class="lead">U bent een bezoeker van de website</p>
                <p><a href="about.php" class="btn btn-md btn-default">Klik hier om terug te keren naar de home pagina.</a></p>
                <p class="lead">U heeft normaal gesproken toegang tot deze pagina</p>
                <p><a href="login.php" class="btn btn-md btn-default">Klik hier om in te loggen.</a></p>
                <p class="lead">Anders? Neem contact op</p>
                <p><a href="contact.php" class="btn btn-md btn-default">Klik hier om contact op te nemen</a></p>
            </div>
        </div>
    </div>
</div>
</body>
</html>