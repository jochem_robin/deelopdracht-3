<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli =  get_mysqli();

// Haal het team ID uit het HTTP request
$teamid = 0;
if(isset($_GET['teamid'])) {
    $teamid = $_GET['teamid'];
}

if(isset($_POST['name'])) {
  $name = $_POST['name'];
  $query = "INSERT INTO `lid` (`id`, `naam`) VALUES (NULL, '$name') ";
  $mysqli->query($query);
  $user_id =  $mysqli->insert_id;
  $update_query = "INSERT INTO `team_has_lid` (`team`, `lid`) VALUES ('$teamid', '$user_id')";
  $mysqli->query($update_query);
}

//wanneer wordt deze uitgevoerd?
//if submit
//welke submit, submit value is delete
if (isset($_GET['lid_id'])) {
  //echo "jippie";
  $id = $mysqli->real_escape_string($_GET['lid_id']);
  $sql_delete = "DELETE FROM team_has_lid WHERE lid = {$id} AND team = {$teamid}";
  echo $sql_delete;
  $mysqli->query($sql_delete) or die ($mysqli->error);

  header("location: team.php?teamid={$teamid}");
  exit();
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">
		<?php
			$sql = "SELECT * FROM TEAM WHERE ID=".$teamid;
			$result = $mysqli->query($sql);
			if($result->num_rows >0) {
				$row = $result->fetch_assoc();
				$teamnaam = $row['naam'];
        $lidnaam = $row['id'];
				echo '<div class="well"><h1>Team '. $teamid . ': '. $teamnaam .'</h1></div>';
		    } ?>
        <?php // select query alles van team_has_lid
          $sql = "SELECT * FROM TEAM_HAS_LID WHERE ID=";
        // wijs variabel aan veld `lid` toe ($lid_id = $row['lid'];)

        //if isset goedzetten
        ?>
         <? //php if(isset( ));
              // sql even god in variabele plaatsen.
             //"DELETE FROM `team_has_lid` WHERE `lid` = $lid_id";
              //mysql_query("DELETE FROM team_has_lid WHERE `lid`=$lid_id");
              //header('Location: main.php'); } ?>

		<div role="tabpanel">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#spelers" aria-controls="spelers" role="tab" data-toggle="tab">De spelers</a></li>
  				<li role="presentation"><a href="#wedstrijden" aria-controls="wedstrijden" role="tab" data-toggle="tab">Wedstrijden</a></li>
  				<li role="presentation"><a href="#uitslagen" aria-controls="uitslagen" role="tab" data-toggle="tab">Uitslagen</a></li>
  				<li role="presentation"><a href="#addspeler" aria-controls="addspeler" role="tab" data-toggle="tab">Speler toevoegen</a></li>
  			</ul>

  			<!-- Tab panes -->
  			<div class="tab-content">
  				<div role="tabpanel" class="tab-pane fade in active" id="spelers">
  					<?php // De spelers van dit team
            // $sql = "SELECT * FROM team_has_lid where team = .$teamid;";

  					$sql = "SELECT L.naam, L.id FROM LID L, TEAM_HAS_LID TL WHERE L.id = TL.lid AND TL.team = $teamid";
  					$resSpelers = $mysqli->query($sql);
            // echo '<pre>' . var_export($resSpelers, true) . '</pre>';

  					if(!$resSpelers || $resSpelers->num_rows == 0 ) {
						echo '<div class="alert alert-warning" role="alert">'.
									'<i class="fa fa-exclamation-triangle"></i> Er zijn geen spelers in dit team</div>';
					} else {
						echo '<table class="table table-striped">';
						while($rowSpeler = $resSpelers->fetch_assoc()) { ?>
							<tr>
								<td class="col-sm-1">
									<i class="fa fa-user fa-3x"></i>
								</td>
								<td class="col-sm-10">
									<strong><?php echo $rowSpeler['naam'] ?></strong><br/>
									<small>Al 3 sezoenen actief</small>
								</td>
                <td class="col-sm-1">
                  <form>
									 <a class="btn btn-default" href= "team.php?<?php echo 'lid_id='.$rowSpeler['id'].'&teamid='.$teamid; ?>">
                      <i class="fa fa-pencil" aria-hidden="true"></i>
                  </form>
								</td>  <td class="col-sm-1">
  									<a class="btn btn-default" href= "team.php?<?php echo 'lid_id='.$rowSpeler['id'].'&teamid='.$teamid; ?>">
                      <i class="fa fa-trash" aria-hidden="true"></i>
                    </a>
  								</td>
							</tr>
						<?php }
						echo "</table>";
					} ?>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="wedstrijden">
					<?php // De wedsrtijden van dit team (team A, Team B of Scheidsrechter
					$sql = "SELECT S.datum, W.tijd, W.veld, TB.naam ".
							"FROM W_TEAM WTA, WEDSTRIJD W, W_TEAM WTB, TEAM TB, SPEELWEEK S ".
							"WHERE WTA.teamid=".$teamid." AND WTA.Rol=1 AND WTA.wedstrijdid=W.id ".
							"AND WTB.wedstrijdid=W.id AND WTB.rol=2 AND WTB.teamid=TB.id ".
							"AND S.id=W.speelweekid ".
							"UNION ".
							"SELECT S.datum, W.tijd, W.veld, TA.naam ".
							"FROM W_TEAM WTB, WEDSTRIJD W, W_TEAM WTA, TEAM TA, SPEELWEEK S ".
							"WHERE WTB.teamid=".$teamid." AND WTB.Rol=2 AND WTB.wedstrijdid=W.id ".
							"AND WTA.wedstrijdid=W.id AND WTA.rol=1 AND WTA.teamid=TA.id ".
							"AND S.id=W.speelweekid ".
							"UNION ".
							"SELECT S.datum, W.tijd, W.veld, '<i>Scheidsrechter</i>' AS naam ".
							"FROM W_TEAM WTS, WEDSTRIJD W, SPEELWEEK S ".
							"WHERE WTS.teamid=".$teamid." AND WTS.Rol=3 ".
							"AND WTS.wedstrijdid=W.id ".
							"AND S.id=W.speelweekid ".
							"ORDER BY datum, tijd";
					$resWedstr = $mysqli->query($sql);
					if(!$resWedstr || $resWedstr->num_rows == 0 ) {
						echo '<div class="alert alert-info" role="alert">'.
									'<i class="fa fa-info-circle"></i> Er zijn geen wedstrijden voor dit team</div>';
					} else {
						?>  <table class="table table-striped">
								<tr>
									<th>Datum</th>
									<th>Tijd</th>
									<th>Veld</th>
									<th>Tegen</th>
								</tr>
							<?php	while($rowWedstrijd = $resWedstr->fetch_assoc()) { ?>
							<tr>
								<td class="col-sm-1">
									<?php echo $rowWedstrijd['datum'] ?><br/>
								</td>
								<td class="col-sm-1">
									<?php echo $rowWedstrijd['tijd'] ?><br/>
								</td>
								<td class="col-sm-1">
									<?php echo $rowWedstrijd['veld'] ?><br/>
								</td>
								<td class="col-sm-11">
									<strong><?php echo $rowWedstrijd['naam'] ?></strong><br/>
								</td>
							</tr>
						<?php }
						echo "</table>";
					}?>
					</div>
				<div role="tabpanel" class="tab-pane fade" id="uitslagen">Hier komen de uitslagen</div>
        <div role="tabpanel" class="tab-pane fade" id="addspeler">Speler toevoegen
          <form method="POST">
            <input type="text" class="form-control" name="name"></input>
            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"> </i> Toevoegen</button>
            <form>


        </div>
				<div role="tabpanel" class="tab-pane fade" id="statistieken">
					<?php // De spelers van dit team
					$sql = "SELECT * FROM STATSVIEW WHERE team = ".$teamid;
					$resStats = $mysqli->query($sql);
					if(!$resStats || $resStats->num_rows == 0 ) {
						echo '<div class="alert alert-warning" role="alert">'.
									'<i class="fa fa-exclamation-triangle"></i> Er zijn geen statistieken in dit team</div>';
					} else {
						$rowStats = $resStats->fetch_assoc();
						?> <table class="table table-striped">
							<tr>
								<td>
									<strong>W</strong><br/>
									<small><i>Aantal gespeelde wedstrijden</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['W'] ?></strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>P</strong><br/>
									<small><i>Competitiepunten (gewonnen sets min strafpunten)</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['P'] ?></strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>Sv</strong><br/>
									<small><i>Scores voor</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['Sv'] ?></strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>St</strong><br/>
									<small><i>Scores tegen</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['St'] ?></strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>S</strong><br/>
									<small><i>Score saldo (Sv - St)</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['S'] ?></strong><br/>
								</td>
							</tr><tr>
								<td>
									<strong>Str.P</strong><br/>
									<small><i>Strafpunten</i></small>
								</td>
								<td>
									<strong><?php echo $rowStats['strp'] ?></strong><br/>
								</td>
							</tr>
						</table>
					<?php } ?>
				</div>
			</div>

		</div>

		</main>
	</body>
</html>
