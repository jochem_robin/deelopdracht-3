<?php
    // Sessies en Autorisatie
    require_once 'tools/security.php';

    // Nodig als de database wordt gebruikt in dit script
	require_once 'tools/db.php';
	$mysqli =  get_mysqli();

	// Speeltijden ophalen
    $tijden = Array();
    $sql = "SELECT * FROM Ronde";
    $resTijden = $mysqli->query($sql);
    while($rowTijd = $resTijden->fetch_assoc()) {
        $tijden[$rowTijd['id']] = $rowTijd['tijd'];
	}
	

	$maandDatum = date ('m');
	$jaarDatum = 2014;
	// Haal het maand & jaar ID uit het HTTP request
	if(isset($_GET['maand'])) {
		$maandDatum = $_GET['maand'];
	}
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">
			<div class="well"><h1>Wedstrijdschema</h1></div>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
				<?php 
					if($maandDatum <= 03) {
						$jaarDatum++;
					}

					$sql= "SELECT * FROM SPEELWEEK WHERE datum LIKE'$jaarDatum-$maandDatum-%'" ;
					$resWeken = $mysqli->query($sql);
					if($resWeken->num_rows == 0) {
						echo '<div class="alert alert-warning" role="alert">'.
									'<i class="fa fa-exclamation-triangle"></i> Er zijn geen speelweken gevonden</div>';
					} else {
						  /*$expanded = " in";*/
						while ($rowWeek = $resWeken->fetch_assoc()) { 
							$date = date("d F Y", strtotime($rowWeek['datum']));
							$panelID = 'heading'.$rowWeek['id'];
							$collapseID = 'collapse'.$rowWeek['id'];
							?>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="<?php echo $panelID ?>">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $collapseID ?>"
											aria-expanded="false" aria-controls="<?php echo $collapseID ?>">
											Speelweek <?php echo $rowWeek['id'].": ".$date ?>
										</a>
									</h4>
								</div>
								<div id="<?php echo $collapseID ?>" class="panel-collapse collapse<?php echo $expanded ?>" role="tabpanel"
									aria-labelledby="<?php echo $panelID ?>">
									<div class="panel-body">
										<?php
                                            $speelweek = $rowWeek['id'];
											$sql = "SELECT wedstrijd.*, team.klasse FROM wedstrijd
                                                        INNER JOIN team ON wedstrijd.team_a = team.id WHERE speelweek = $speelweek";
											$resWedstr = $mysqli->query($sql);
											if(!$resWedstr || $resWedstr->num_rows == 0) {
												echo '<div class="alert alert-info" role="alert">'.
															'<i class="fa fa-info-circle"></i> Er zijn geen wedstrijden gevonden</div>';
											} else {
										?>
										<table class="table table-condensed table-striped">
											<tr>
												<th class="col-sm-1">Tijd</th>
												<th class="col-sm-1">Veld</th>
												<th class="col-sm-1">Klas</th>
												<th class="col-sm-3">Team A</th>
												<th class="col-sm-3">Team B</th>
												<th class="col-sm-2">Scheidsrechter/teller</th>
												<th class="col-sm-1">Klas</th>
												<th></th>
											</tr>
											<?php

											$teams = Array ();
											$sql = "SELECT * FROM Team";
											$resNamen = $mysqli->query($sql);
											while($rowTeam = $resNamen->fetch_assoc()){
												$teams[$rowTeam['id']] = $rowTeam['naam'];
											}
											
											$klas = Array ();
											$sql = "SELECT * FROM Klas";
											$resKlas = $mysqli->query($sql);
											while($rowKlas = $resKlas->fetch_assoc()) {
												$klas[$rowKlas['code']] = $rowKlas['naam'];
											}
											


												while($rowWedstr = $resWedstr->fetch_assoc()) {
													echo "<tr>";
													echo '<td>'.$tijden[$rowWedstr['ronde']]."</td>";
													echo "<td>".$rowWedstr['veld']."</td>";
													echo "<td>".$klas[$rowWedstr['klasse']]."</td>";
													echo "<td><strong>".$teams[$rowWedstr['team_a']]."</td>";
													echo "<td><strong>".$teams[$rowWedstr['team_b']]."</strong></td>";
													echo "<td>".$teams[$rowWedstr['scheids']]."</td>";
													echo "<td>".$klas{$rowWedstr['klasse']}."</td>";
													echo "<td>";


													// Kijken of er nog geen uitslag is
													$wedstrijd_id = $rowWedstr['id'];
													$sql = "SELECT COUNT(*) AS uitslag FROM Uitslag_set WHERE wedstrijd=$wedstrijd_id";
													$res = $mysqli->query($sql);
													$row = $res->fetch_assoc();

                          							if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true && $row['uitslag']==0) {
															echo '<a href="invullenuitslag.php?wedstrijdid='.$wedstrijd_id.'">Uitslag</a>';
													}
													 
													echo "</td>";
													echo "</tr>";
												}
											?>
										</table>
										<?php } // end if ?>
									</div>
								</div>

								
							</div>				
											

							<?php 
							
						}
					}
				?>		
				

				<?php
					$vorige = $maandDatum - 01;
					if($vorige == 00) {
						$vorige = 12;
					}
					$vorigePadded = sprintf("%02d", $vorige);

					if($vorigePadded >= '09' or $vorigePadded <= '03') {
					echo '<a class="btn btn-primary" href="?maand='.$vorigePadded.'">Vorige</a>';
					}
                ?>


                <?php
					$volgende = $maandDatum + 01;
					if($volgende == 13) {
						$volgende = 01;
					}

					$volgendePadded = sprintf("%02d", $volgende);
					
					if($volgendePadded >= '09' or $volgendePadded <= '03')
					echo '<a class="btn btn-primary" href="?maand='.$volgendePadded.'">Volgende</a>';
                ?>


				<?php
				    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
					echo '<a class="btn btn-default" href="generate.php">Genereer Wedstrijden</a>';
					}
				?>
		</main>
	</body>
</html>