<?php
// Sessies en Autorisatie
require_once 'tools/security.php';

// Nodig als de database wordt gebruikt in dit script
require_once 'tools/db.php';
$mysqli =  get_mysqli();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well"><h1>Uitslagen</h1></div>

			<table class="table table-striped">
					<tr>
						<th>Wedstrijd</th>
						<th>Set</th>
						<th>Score</th>
						<th></th>
						<th>Punten</th>
						<th></th>
					</tr>

            <?php
			$con=mysqli_connect("localhost","root","","mbvolley");
				// Check connection
				if (mysqli_connect_errno())
				{
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
				}

				$result = mysqli_query($con,"SELECT * FROM uitslag_set");

					while($row = mysqli_fetch_array($result))
					{
					echo "<tr>";

						echo "<td>" . $row['wedstrijd'] . "</td>";
						echo "<td>" . $row['sets'] . "</td>";
						echo "<td>" . $row['score_a'] . "</td>";
						echo "<td>" . $row['score_b'] . "</td>";
						echo "<td>" . $row['punten_a'] . "</td>";
						echo "<td>" . $row['punten_b'] . "</td>";

					echo "</tr>";
					}

				mysqli_close($con);
			?>

			</table>

		</main>
	</body>
</html>
 